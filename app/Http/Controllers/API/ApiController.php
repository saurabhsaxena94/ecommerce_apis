<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class ApiController extends Controller
{
	public $successStatus = 200;

	/** 
    * login api 
    * 
    * @return \Illuminate\Http\Response 
    */ 
	public function login(Request $request)
	{ 
		$validator = Validator::make($request->all(), [
			'email' => 'required|email', 
			'password' => 'required', 
		]);
		if ($validator->fails()) 
		{ 
			return response()->json(['status' => 401, 'message' => $validator->errors()]);            
		}
		if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
			$user = Auth::user(); 
			$success['token'] =  $user->createToken('MyApp')->accessToken; 
			return response()->json(['status' => $this->successStatus, 'token' => $success['token'], 'message' => 'User Logged In successfully.']); 
		} 
		else
		{ 
			return response()->json(['status' => 401, 'message'=>'E-Mail or Password is incorrect.']); 
		} 
	}

	/**
    * Logout user (Revoke the token)
    *
    * @return [string] message
    */
    public function logout(Request $request)
    {   
        $request->user()->token()->revoke();
        return response()->json(['success' => $this->successStatus, 'message' => 'Successfully logged out']);
    }

	/** 
    * Register api 
    * 
    * @return \Illuminate\Http\Response 
    */ 
	public function register(Request $request) 
	{
		$validator = Validator::make($request->all(), [ 
			'name' => 'required', 
			'email' => 'required|email|unique:users', 
			'password' => 'required', 
			'c_password' => 'required|same:password', 
		]);
		if ($validator->fails()) { 
			return response()->json(['status' => 401, 'message' => $validator->errors()]);            
		}
		$input = $request->all(); $input['password'] = bcrypt($input['password']); 
		$user = User::create($input); 
		$success['token'] =  $user->createToken('MyApp')->accessToken; 
		$success['name'] =  $user->name;
		return response()->json(['status' => $this->successStatus, 'token' => $success['token'], 'name' => $success['name'], 'message' => 'User created successfully.']);
	}

	/** 
    * details api 
    * 
    * @return \Illuminate\Http\Response 
    */ 
	public function details() 
	{ 
		$user = Auth::user(); 
		return response()->json(['status' => $this->successStatus, 'details' => $user, 'message' => 'User info fetched successfully.']); 
	} 
}
